/**
 * @version : 1.0
 * Ksensetech development : Knack Application 
 *
 * @author  : Kelson Erwin
 * @author  : Gianpiere Ramos
 *
 */ 

"use strict";

var KsensetechApp = {};
var KnackApp = function(API_KEY, APP_ID){

	this.config = {
		ajax : {
			async : false, // wait request (Async)
			dataType : 'json' // data type request
		}

	}
	
	this.headers = {
		Authorization: Knack.getUserToken(),
		"X-Knack-Application-Id": APP_ID||Knack.application_id,
		"X-Knack-REST-API-Key": API_KEY,
		"Content-Type": "application/json",
		"x-knack-new-builder": "true",
		"X-Requested-With": "XMLHttpRequest"
	};

	// xhr Ajax Headers
	this.xhrHeaders = function(headers){ Object.assign(this.headers,headers||{}) };

};

typeof Knack === 'undefined' ? 
(function(){
	Knack.viewData = function(view){
		console.error( typeof Knack === 'undefined' ?
		 	'Sorry, the Knack Object is undefined' : 
		 	'Sorry, the Knack prototype viewData function is not implement'
		 );
	};
})() :
(function(){
	
	function KnackConsoleError(title, message){ console.error(`${title}\n`, message); };

	/**
	 * @package - viewData
	 * @description	
	 | 
	 |	Knack view data manipulation 
	 |  
	 | 
	 *
	 */
	Knack._SessionIteratator = function(index){

		var sessionIndex = String('ssknack_'+index);
		var data = sessionStorage.getItem(sessionIndex);

		return {
			value : data,
			get : function(){
				return sessionStorage.getItem(sessionIndex);
			},
			set : function(value){
				sessionStorage.setItem(sessionIndex, value);
			},
			delete : function(){
				sessionStorage.removeItem(sessionIndex);
			},
			clearSession : function(){

			}
		}
	};

	/**
	 * @package - viewData
	 * @description	
	 | 
	 |	Knack view data manipulation 
	 |  
	 | 
	 *
	 */
	Knack.viewData = function(view){

		if( typeof Knack.views[view] !== 'object' )
			KnackConsoleError(
				'Knack viewData function Error', 
				'The viewData function is not defined'
			);

		var _view  = Knack.views[view].model.view||false;
		var models = Knack.views[view].model.data.models||[];

		let fieldsByName = {};

		function _rowProcessKeyToName(){
			if(typeof _view === 'object' && typeof _view.fields === 'object'){
				var obj = _view.fields;
				var newObj = {};

				if(typeof obj === 'object'){
					for (var i in obj){
						fieldsByName[obj[i].name] = obj[i].key;
					}
				}
			}
		}; _rowProcessKeyToName();

		return {
			// Attributes
			result 	: models,
			lenght 	: models.lenght,

			// Methods
			get 	: function(index){
				return models[index].attributes;
			},
			index 	: function(index){
				return {
					fields : models[index].attributes,
					field : function(field_id){
						return -1!==String(field_id).indexOf('_raw') 
						&& typeof models[index].attributes[field_id] === 'object'
						?models[index].attributes[field_id][0]
						:(typeof models[index].attributes[field_id]!=='undefined'?models[index].attributes[field_id]:{});
					},
					fieldByName : function(name){
						return typeof fieldsByName[name] !== 'undefined' 
						? models[index].attributes[fieldsByName[name]]
						: undefined;
					},
				}
			},
			scene : function(){
				return _view !== false ? _view.scene : false; 
			}
		}
	};

	/**
	 * @package - KnackRecords
	 * @description	
	 | 
	 |	Knack view data manipulation 
	 |  
	 | 
	 *
	 */
	Knack.KnackRecords = function(knack_records){

		if( typeof knack_records !== 'object' )
			KnackConsoleError(
				'Knack KnackRecords function Error', 
				'The KnackRecords params is not records object'
			);

		var records = knack_records||{};
		return {
			// Attributes
			records	: records,
			lenght 	: records.lenght,

			// Methods
			getIndex 	: function(index){
				return {
					fields : records[index].attributes,
					field : function(field_id){
						return -1!==String(field_id).indexOf('_raw')?records[index].attributes[field_id][0]:records[index].attributes[field_id];
					},
					fieldName : function(){
						console.warn('Sorry, Method in Construction.');
						// in construction
					}
				}
			},
			record : function(index){
				return records[index] !== undefined ? records[index] : '';
			},
		}
	};

	/**
	 * @package - tool application method
	 * @param : (string) objectId 
	 * @param : (string) or (object) filters 
	 * @param : (bool) encode 
	 * @return: url compose
	 *
	 * @description	
	 | 
	 |	Create Url form api knack ajax object 
	 | 
	 *
	 */
	Knack.encodeObjectId = function(objectId, filters = '', encode = false){
		let _filter = '?builder_table_per_page=10000&builder_table_page=1'+filters?'&'+encode?encodeURIComponent(filters):filters:'';
		return Knack.objects.url.replace(/.$/,"/"+objectId+'/records/'+_filter);
	}


	/**
	 * @package - KnackApp
	 * @param : (string) url
	 * @param : (object) args
	 * @param : (function) callback 
	 * @param : (bool) stryngify json stryngify true|false
	 * @param : (bool) async wait or async request true|false
	 * @return: response object
	 *
	 * @description	
	 | 
	 |	Extend the KnackApp Object attributes and methods
	 | 
	 *
	 */
	KnackApp.prototype.get = function(Url, args, callback, stryngify, async = undefined){

		let config = this.config.ajax;

		window.$.ajax({
			url : Url,
			type: 'GET',
			headers : this.headers,
          	dataType: config.dataType||'json',
			async: async!=='undefined'?async:config.async||true,
			data:stryngify===true?JSON.stryngify(args):args||{},
			beforeSend: function(request) {
				request.setRequestHeader("DevelopmentBy", 'Ksensetech');
			},
			success : function(response){ 
				if(async === true || callback !== 'function') 
					return response; callback(response); 
			},
			error 	: function(jqXHR, exception){
				console.error("Ajax Response Error:\n", { status : jqXHR.status, statusText : jqXHR.statusText, message : jqXHR.responseText, exception : exception });
			}
		});
	}

	/**
	 * @package - KnackApp
	 * @param : (string) objectId
	 * @param : (object) args
	 * @param : (function) callback 
	 * @param : (bool) stryngify json stryngify true|false
	 * @param : (bool) async wait or async request true|false
	 * @return: response object
	 *
	 * @description	
	 | 
	 |	Extend the KnackApp Object properties
	 | 
	 *
	 */
	KnackApp.prototype.ObjectId = function(objectId, stryngify, async = undefined){

		let ObjID = objectId;
		let config = this.config.ajax;
		let _headers = this.headers;

		console.log('this: %o', this);


		function _InternalAjax(Url, args, callback){
			return {
				url : Url,
				type: 'GET',
				headers : _headers,
	          	dataType: config.dataType||'json',
				async: async!=='undefined'?async:config.async||true,
				data:stryngify===true?JSON.stryngify(args):args||{},
				beforeSend: function(request) {
					request.setRequestHeader("DevelopmentBy", 'Ksensetech');
				},
				success : function(response){ 
					if(async === true || typeof callback !== 'function') 
						return response; 

					callback(response); 
				},
				error 	: function(jqXHR, exception){
					console.error("Ajax Response Error:\n", { status : jqXHR.status, statusText : jqXHR.statusText, message : jqXHR.responseText, exception : exception });
				}
			};
		}

		let _filter = '';
			_filter = '?builder_table_per_page=10000&builder_table_page=1';

		return {
			filter : function(filters = '', encode = false){
				_filter = '?builder_table_per_page=10000&builder_table_page=1'+filters?'&'+encode?encodeURIComponent(filters):filters:'';
			},
			get : function(args, callback){
				let url = Knack.objects.url.replace(/(.)$/g,"$1\/"+ObjID+'/records/'+_filter);
				window.$.ajax(_InternalAjax(url, args, callback));
			},
			set : function(args, callback){
				let url = "https://us-east-1-builder-write.knack.com/v1/objects/:id/records/".replace(':id', ObjID + _filter);
				window.$.ajax(_InternalAjax(url, args, callback));
			},
			del : function(args, callback){
				let url = Knack.objects.url.replace(/(.)$/g,"$1\/"+ObjID+'/records/delete/'+_filter);
				window.$.ajax(_InternalAjax(url, args, callback));
			},
			upd : function(args, callback){
				let url = "https://us-east-1-builder-write.knack.com/v1/objects/:id/records/".replace(':id', ObjID + _filter);
				window.$.ajax(_InternalAjax(url, args, callback));
			}
		}

	}



})();



// Ksensetech company
var stl={st_1s:"font-size:5em;font-weight:bold;font-family:Arial;",st_1c:"color:#268ffb;",st_2c:"color:#2aaff3;",st_2s:"font-size:2em;color:gray;margin-bottom:20px;",flbl:"\n"+String.fromCharCode(64,100,101,118,101,108,111,112,112,101,114,32,115,111,102,116)};console.log("%c%s%c%s%c%s",stl.st_1s+stl.st_1c,String.fromCharCode(75),stl.st_1s+stl.st_2c,String.fromCharCode(115,101,110,115,101,116,101,99,104),stl.st_2s,stl.flbl); try{Knack.$.fn.extend({KsenseTechTableHeaderToFront:function(){t=this;var n=Knack.$(t).children("thead");n.length>0&&Knack.$(t).append(n)}});}catch(e){}
