/** 
 * ksensetech
 * @see 	: https://ksensetech.com/knack/developers
 * author 	: Ksensetech Developments
 * author 	: Kelson Erwin
 * author 	: Gianpiere Ramos
 * created 	: 14-06-2019
 * update 	: 14-06-2019
 */

var $fn = Knack.$.fn; window.pg = (window.pg||{});

// required : https://code.jquery.com/jquery-2.2.4.min.js

$fn.extend({
    'tableNavigation': function (options) {

        function KsensetechTableNavigation(id, st){
            
            if( typeof options === 'string' ){
                switch(options){
                    case 'destroy' : 
                        break;
                    case 'disabled': 
                        break;
                    case 'enabled' : 
                        break;
                    case 'disableEvents' : 
                        break;
                    case 'enableEvents' : 
                        break;
                    default:
                        break;
                }
            }

            var $table = $(`#${id} table`);

            $table.addClass('table-navigation');

            var defaults = {
                navigation_row : true,
                navigation_columns : true,
                focus_class : '',
                focus_class : '',
                focus_start: false,
                focus_start_row_index : 0,
                trigger_event_key : '{SPACE}',
                init : function(args){},
                beforeStarted : function(){},
                afterStarted  : function(){},

                CellOnFocus : function(){},
                CellFocusOut: function(){},
            };

            this.setting = Object.assign(defaults, options);

            // body rows 
            $tbody = $table.find('tbody');

            // document event
            $(document).off('mousedown').on('mousedown', function(){ 
                try{
                    //$table.removeClass('focus');
                    $('.table-navigation').removeClass('focus');
                    $('.table-navigation .row-focus').removeClass('row-focus');
                    $('.table-navigation .row-focus .cell-selected').removeClass('cell-selected');

                    $('.wrap-table-navigation-focus').removeClass('wrap-table-navigation-focus');
                }catch(e){ }

                /*window.addEventListener("keydown", function(event) {
                    // arrow key
                    if ([37, 38, 39, 40].indexOf(event.keyCode) > -1) {
                        event.preventDefault();
                    }
                });*/

            });


            function scrollIf(type){
                if(type == 'down'){
                    var c_h = $('.cell-selected').offset().top + $('.cell-selected').innerHeight(), w_h = window.innerHeight + window.scrollY;
                    if(c_h >= w_h){
                        window.scrollTo(window.scrollX, $('.cell-selected').offset().top - (window.innerHeight - $('.cell-selected').innerHeight() - 75 ) );
                    }
                }else if(type == 'up'){
                    if(window.scrollY >= $('.cell-selected').offset().top){
                        window.scrollTo(window.scrollX, $('.cell-selected').offset().top - $('.cell-selected').innerHeight());
                    }
                }

                return true;

            }

            // document event
            $(document).off('keyup').on('keyup', function(e){
                var cellIndex = 0;
                try{

                    if(e.key.toLowerCase() === 'c' && e.ctrlKey){
                        /*$('#sl_text_copy').select();*/
                       /* document.execCommand('copy');*/ 
                        console.info('text copied');
                    }

                    switch(e.key){
                        case "ArrowUp" : 
                            e.preventDefault();
                            scrollIf('up');
                            if($('.table-navigation.focus .row-focus').eq(0).prev().length !== 0){
                                cellIndex = parseInt(Knack.$('.cell-selected').index()||0);
                                var $newEl = $('.table-navigation.focus .row-focus').eq(0).prev();
                                $newEl.addClass('row-focus').siblings().removeClass('row-focus');
                                $('.table-navigation.focus .cell-selected').removeClass('cell-selected');
                                if($newEl.find('td:eq('+cellIndex+')').length > 0){
                                    $newEl.find('td:eq('+cellIndex+')').addClass('cell-selected').siblings().removeClass('cell-selected');
                                }else{
                                    $newEl.find('td:eq(0)').addClass('cell-selected').siblings().removeClass('cell-selected');
                                }
                            }else{
                                var id_ = $('.table-navigation.focus').parent().parent().attr('id');
                                window.pg[id_] = Knack.views[id_].model.view.pagination_meta;
                                if(window.pg[id_].page > 1){
                                    // save row position before load new page.
                                    sessionStorage.setItem('NavigatePage-save-cell-position', JSON.stringify({view_id : id_, type : 'up'}) );
                                    Knack.views[id_].changePage(parseInt(window.pg[id_].page-1)); // prev page 
                                }

                            };
                            break;
                        case "ArrowDown" :
                            e.preventDefault();
                            scrollIf('down');
                            if($('.table-navigation.focus .row-focus').eq(0).next().length !== 0){
                                cellIndex = parseInt(Knack.$('.cell-selected').index()||0);
                                var $newEl = $('.table-navigation.focus .row-focus').eq(0).next();
                                $newEl.addClass('row-focus').siblings().removeClass('row-focus');
                                $('.table-navigation.focus .cell-selected').removeClass('cell-selected');
                                if($newEl.find('td:eq('+cellIndex+')').length > 0){
                                    $newEl.find('td:eq('+cellIndex+')').addClass('cell-selected').siblings().removeClass('cell-selected');
                                }else{
                                    $newEl.find('td:eq(0)').addClass('cell-selected').siblings().removeClass('cell-selected');
                                }
                                // $newEl.find('td:eq('+cellIndex+')').addClass('cell-selected').siblings().removeClass('cell-selected');
                            }else{
                                var id_ = $('.table-navigation.focus').parent().parent().attr('id');
                                window.pg[id_] = Knack.views[id_].model.view.pagination_meta;
                                if((window.pg[id_].page*window.pg[id_].rows_per_page) < window.pg[id_].total_entries === true){
                                    // save row position before load new page.
                                    sessionStorage.setItem('NavigatePage-save-cell-position', JSON.stringify({view_id : id_, type : 'down'}) );
                                    Knack.views[id_].changePage(parseInt(window.pg[id_].page+1)); // next page 
                                }
                            };
                            break;
                        case "ArrowLeft" :
                            if($('.table-navigation.focus .row-focus').eq(0).next().length !== 0){
                                var $newEl = $('.table-navigation.focus .cell-selected').eq(0).prev();
                                $newEl.addClass('cell-selected').siblings().removeClass('cell-selected');
                            }else{
                                // code ..
                            };
                            break;
                        case "ArrowRight" :
                            if($('.table-navigation.focus .cell-selected').eq(0).next().length !== 0){
                                var $newEl = $('.table-navigation.focus .cell-selected').eq(0).next();
                                $newEl.addClass('cell-selected').siblings().removeClass('cell-selected');
                            }else{
                                // code ..
                            };
                            break;
                        case "Escape":
                            console.log('scape.. : %o', $('.kn-popover.drop-element'));
                            try{ 
                                $('.kn-popover.drop-element').remove(); 
                            }catch(e){}
                            break;
                        case " " :
                        case "Enter" :
                            e.preventDefault();
                            var modal = $('.cell-selected a:eq(0)');
                            if(typeof modal !== 'undefined' && modal.length > 0  && modal.attr('href') !== 'undefined'){
                                window.location.replace(modal.attr('href'));
                            }else{ $('.table-navigation.focus .cell-selected').trigger('click'); }
                            break;
                        default : 
                            console.log('is default: %o', e);
                            break;
                    }

                }catch(e){ }
            });

            Knack.$('.table-navigation').off('click').on('click', function(){
                $table.addClass('focus');
                $table.parent().addClass('wrap-table-navigation-focus');
            });

            // table focus
            Knack.$('.table-navigation').off('keyup').on('keyup', function(e){
                // action
                $this = $(this);
            });

            Knack.$('.table-navigation').off('mouseenter').on('mouseenter', function(){
                console.log('enter mouse');
                $this = $(this);
                try{ Knack.$('.table-navigation.focus').removeClass('focus'); }catch(e){};
                $this.addClass('focus');
                $('.cell-selected').parent().addClass('row-focus');
            });

            $('.table-navigation.focus')
                .off('mouseup').on('mouseup', function(){
                    $this = $(this);
                    $this.addClass('focus');
                })
                .off('keyup').on('keyup', function(e){
                    $this = $(this);
                    if( $this.hasClass('focus') ){
                        // ..
                    }
                })
            ;

            // tr navigation
            $tbody.find('tr')
            .off("click").on("click", function(e){
                $row = $(this);
                $('.row-focus').removeClass('row-focus');
                $row.addClass('row-focus');
            });

            $tbody.find('tr td').on("click", function(e){
                $cell = $(this);
                $('.cell-selected').removeClass('cell-selected');
                $cell.addClass('cell-selected');

                // reset edited box
                try{
                    setTimeout(function(){
                        $('.kn-popover.drop-element').remove();
                    }, 200);
                }catch(e){ }

                // copy text
                var text = $cell.text().trim();
                var $input = $('<input type=text>');

                try{$('#sl_text_copy').remove();}catch(e){}

                $input.attr('id', 'sl_text_copy');
                $input.prop('value', text);
                $input.insertAfter($(this));
                $input.hide();
                $input.focus();
                $input.select();

            });

        };

        var $view = Knack.$(this), view_id = $view.attr('id');

        return new KsensetechTableNavigation(view_id, options);

    }
});